//
//  GameScene.m
//  proj6
//
//  Created by Yash Panchamia on 4/6/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "GameScene.h"

SKSpriteNode*player;
@implementation GameScene {
    SKShapeNode *_spinnyNode;
    SKLabelNode *_label;
}

SKNode *bgLayer;
//SKNode *Blocks;
//SKNode *points;
SKNode *gameLayer;
SKSpriteNode *bg1;
SKSpriteNode *bg2;
SKSpriteNode *redCar;
SKSpriteNode *orangeCar;

- (void)didMoveToView:(SKView *)view {
    // Setup your scene here
    
    // Get label node from scene and store it for use later
    _label = (SKLabelNode *)[self childNodeWithName:@"//helloLabel"];
    
    _label.alpha = 0.0;
    [_label runAction:[SKAction fadeInWithDuration:2.0]];
    
    CGFloat w = (self.size.width + self.size.height) * 0.05;
    
    // Create shape node to use during mouse interaction
    _spinnyNode = [SKShapeNode shapeNodeWithRectOfSize:CGSizeMake(w, w) cornerRadius:w * 0.3];
    _spinnyNode.lineWidth = 2.5;
    
    [_spinnyNode runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:M_PI duration:1]]];
    [_spinnyNode runAction:[SKAction sequence:@[
                                                [SKAction waitForDuration:0.5],
                                                [SKAction fadeOutWithDuration:0.5],
                                                [SKAction removeFromParent],
                                                ]]];
    
    gameLayer = [SKNode node];
    [self addChild:gameLayer];
    //BACKGROUND
    SKTexture *bg = [SKTexture textureWithImageNamed:@"trackss"];
    bg1 = [[SKSpriteNode alloc] initWithTexture:bg];
    bg2 = [[SKSpriteNode alloc] initWithTexture:bg];
    bg1.position = CGPointMake(0, 0);
    bg1.zPosition = bg2.zPosition = -20;
    //[bg1 setScale:1.05];
    [bg1 setSize:CGSizeMake(self.size.width, bg1.size.height)];
    bg2.position = CGPointMake(0, bg2.size.height/2-1);
    [bg2 setSize:CGSizeMake(self.size.width, bg2.size.height)];
    [bgLayer addChild:bg2];
    [bgLayer addChild:bg1];

}


- (void)touchDownAtPoint:(CGPoint)pos {
    SKShapeNode *n = [_spinnyNode copy];
    n.position = pos;
    n.strokeColor = [SKColor greenColor];
    [self addChild:n];
}

- (void)touchMovedToPoint:(CGPoint)pos {
    SKShapeNode *n = [_spinnyNode copy];
    n.position = pos;
    n.strokeColor = [SKColor blueColor];
    [self addChild:n];
}

- (void)touchUpAtPoint:(CGPoint)pos {
    SKShapeNode *n = [_spinnyNode copy];
    n.position = pos;
    n.strokeColor = [SKColor redColor];
    [self addChild:n];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Run 'Pulse' action from 'Actions.sks'
    [_label runAction:[SKAction actionNamed:@"Pulse"] withKey:@"fadeInOut"];
    
    for (UITouch *t in touches) {[self touchDownAtPoint:[t locationInNode:self]];}
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *t in touches) {[self touchMovedToPoint:[t locationInNode:self]];}
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {[self touchUpAtPoint:[t locationInNode:self]];}
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {[self touchUpAtPoint:[t locationInNode:self]];}
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
}

@end
